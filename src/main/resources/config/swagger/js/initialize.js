/**
 * @since       2022.03.21
 * @author      preah
 * @description initialize
 **********************************************************************************************************************/
const FN = function () {

    let authorize;
    $.getJSON("./../json/environment.json", function (data) {
        $(".logo__title").text(data.swagger.title);
        document.title = data.swagger.title;
        authorize      = data.authorize;
    });

    addEventListener('keydown', function(e) {
        if (e.keyCode === 113 ) {
            document.getElementsByClassName('authorize__btn')[0].click();
        }
        if (e.keyCode === 114 ) {
            FN.getAccessToken();
        }
        if (e.keyCode === 115 ) {
            document.getElementsByClassName('auth__button auth_submit__button')[0].click();
        }
        if (e.keyCode === 112 ) {

            document.getElementsByClassName('authorize__btn')[0].click();

            setTimeout(() => {

                if(1 != document.getElementsByClassName('auth__button auth_submit__button').length){
                    location.reload();
                    return false;
                }

                if(_.isEmpty($("#url option:selected").val()) || _.isEmpty($("input[name='identify']").val()) || _.isEmpty($("input[name='password']").val())) {
                    alert("url / identify / password is empty");
                    return;
                }

                const target = authorize.hosts.find(host => host.type == $("#url option:selected").val());
                $("#tokenBtn").text("Loding...........");
                $("#tokenBtn").attr("disabled", true);
                $.ajax({
                    url     : target.url,
                    timeout : 3000,
                    dataType: "json",
                    type    : "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization", target.authorization);
                        xhr.setRequestHeader("Content-Type",  "application/x-www-form-urlencoded");
                    },
                    data : {
                        "grant_type" : "password",
                        "client_id"  : target.clientId,
                        "username"   : $("input[name='identify']").val(),
                        "password"   : $("input[name='password']").val(),
                    },
                    success: function(response) {
                        $("input[name='apiKey']").val("bearer ".concat(response.access_token));
                        $("input[name='apiKey']").change();
                        document.getElementsByClassName('auth__button auth_submit__button')[0].click();
                    },
                    error: function(errorThrown) {
                        $("input[name='apiKey']").val("");
                        $("input[name='apiKey']").change();
                        alert("Failed to get authentication information.");
                    },
                    complete: function () {
                        $("#tokenBtn").text("Access Token");
                        $("#tokenBtn").attr("disabled", false);
                    }
                });
            }, 500);
        }
    });

    return {
        initialize: () => {
            const mutationObserver = new MutationObserver((record, observer)=> {
                const dialog = $(".api-popup-dialog-wrapper");
                if(dialog.html() && $("input[name='apiKey']").val() == '') {
                    FN.addUserForm();
                }
            });
            mutationObserver.observe(document.querySelector("#swagger-ui-container"), { childList : true });
        },
        addUserForm:() => {
            const dialog = $(".api-popup-dialog-wrapper");
            dialog.width(640);
            const tokenArea  = "<div class='key_input_container'><h3 class='auth__title'>Api access token</h3><div class='auth__description'></div><div id='user'></div></div>";
            const table = "<table style='width: 100%;'>" +
                "<tr><td colspan='3'><select id='url' multiple style='width: 100%;'></select></td><tr/>"+
                "<tr>" +
                "<td><span class='key_auth__label'>identify: </span><span class='key_auth__value'><input placeholder='identify' class='auth_input' name='identify' type='text'></span></td>" +
                "<td><span class='key_auth__label'>password: </span><span class='key_auth__value'><input placeholder='password' class='auth_input' name='password' type='password'></span></td>" +
                "<td><button type='button' id='tokenBtn' style='width:100%;' onclick='FN.getAccessToken()'>Access Token</button></td>" +
                "</tr>" +
                "<tr>" +
                "<td colspan='3'>* 단축키(<b>F2</b>:인증 레이어 띄우기, &nbsp;<b>F3</b>:엑세스토큰 생성, &nbsp;<b>F4</b>:인증처리, &nbsp;<b>F1</b>:이 모든것을 함께(1+2+3))</td>" +
                "</tr>" +
                "</table>";

            $(dialog).find("div .key_input_container").before(tokenArea);
            $("#user").append(table);

            authorize.hosts.forEach((el, index) => {
                let option = {value:el.type, text:'['+el.type+'] '+el.url, selected: -1 < $(location).attr('host').indexOf(el.pattern)}
                $("#url").append($("<option>", option));
            });
        },
        getHost:() => {
            return authorize.hosts.find(host => host.pattern.includes($(location).attr('host')));
        },
        getAccessToken: () => {
            if(_.isEmpty($("#url option:selected").val()) || _.isEmpty($("input[name='identify']").val()) || _.isEmpty($("input[name='password']").val())) {
                alert("url / identify / password is empty");
                return;
            }

            const target = authorize.hosts.find(host => host.type == $("#url option:selected").val());
            $("#tokenBtn").text("Loding..");
            $("#tokenBtn").attr("disabled", true);
            $.ajax({
                url     : target.url,
                timeout : 3000,
                dataType: "json",
                type    : "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", target.authorization);
                    xhr.setRequestHeader("Content-Type",  "application/x-www-form-urlencoded");
                },
                data : {
                    "grant_type" : "password",
                    "client_id"  : target.clientId,
                    "username"   : $("input[name='identify']").val(),
                    "password"   : $("input[name='password']").val(),
                },
                success: function(response) {
                    $("input[name='apiKey']").val("bearer ".concat(response.access_token));
                    $("input[name='apiKey']").change();
                },
                error: function(errorThrown) {
                    $("input[name='apiKey']").val("");
                    $("input[name='apiKey']").change();
                    alert("Failed to get authentication information.");
                },
                complete: function () {
                    $("#tokenBtn").text("Access Token");
                    $("#tokenBtn").attr("disabled", false);
                }
            });
        }
    };
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});