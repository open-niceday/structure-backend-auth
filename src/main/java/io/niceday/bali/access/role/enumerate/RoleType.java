package io.niceday.bali.access.role.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2022.11.24
 * @author      lucas
 * @description 권한 타입
 *               - ROLE_COMMON          : 공통
 *               - ROLE_ADMIN_MASTER    : 관리자-마스터
 *               - ROLE_ADMIN_MANAGER   : 관리자-매니저
 *               - ROLE_TIC_SUPERVISOR  : 사용자-슈퍼바이저
 *               - ROLE_TIC_USER        : 사용자-유저
 *               - ROLE_ADMIN_HOTEL     : 관리자-숙소관리
 *               - ROLE_ADMIN_ACCOUNT   : 관리자-사용자관리
 *               - ROLE_ADMIN_PARTNER   : 관리자-파트너지원
 *               - ROLE_USER_DASHBOARD  : 사용자-대시보드
 *               - ROLE_USER_SELL       : 사용자-판매관리
 *               - ROLE_USER_PROMOTION  : 사용자-프로모션
 *               - ROLE_USER_RESERVE    : 사용자-예약관리
 *               - ROLE_USER_HOTEL      : 사용자-숙소관리
 *               - ROLE_USER_PARTNER    : 사용자-파트너지원
 *               - ROLE_USER_ROOM_HARD  : 사용자-하드블록관리
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum RoleType {

     ROLE_COMMON        ("공통")
    ,ROLE_ADMIN_MASTER  ("관리자-마스터")
    ,ROLE_ADMIN_MANAGER ("관리자-매니저")
    ,ROLE_TIC_SUPERVISOR("사용자-슈퍼바이저")
    ,ROLE_TIC_USER      ("사용자-유저")
    ,ROLE_ADMIN_HOTEL   ("관리자-숙소관리")
    ,ROLE_ADMIN_ACCOUNT ("관리자-사용자관리")
    ,ROLE_ADMIN_PARTNER ("관리자-파트너지원")
    ,ROLE_USER_DASHBOARD("사용자-대시보드")
    ,ROLE_USER_SELL     ("사용자-판매관리")
    ,ROLE_USER_PROMOTION("사용자-프로모션")
    ,ROLE_USER_RESERVE  ("사용자-예약관리")
    ,ROLE_USER_HOTEL    ("사용자-숙소관리")
    ,ROLE_USER_PARTNER  ("사용자-파트너지원")
    ,ROLE_USER_ROOM_HARD("사용자-하드블록관리");

    private String description;
}
