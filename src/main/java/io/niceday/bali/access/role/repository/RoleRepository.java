package io.niceday.bali.access.role.repository;

import io.niceday.bali.access.role.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role repository
 **********************************************************************************************************************/
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, QuerydslPredicateExecutor<Role> {

}
