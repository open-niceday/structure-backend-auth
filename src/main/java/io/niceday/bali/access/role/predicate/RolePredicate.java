package io.niceday.bali.access.role.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.bali.access.role.entity.QRole;
import io.niceday.bali.access.role.form.RoleForm.Request;

import java.util.Optional;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role predicate
 **********************************************************************************************************************/
public class RolePredicate {
	
	public static Predicate search(Request.Find find) {

		QRole 		   role    = QRole.role;
		BooleanBuilder builder = new BooleanBuilder();

		Optional.ofNullable(find.getRoleType()).ifPresent(p -> builder.and(role.roleType.eq(p)));

		return builder;
	}
}