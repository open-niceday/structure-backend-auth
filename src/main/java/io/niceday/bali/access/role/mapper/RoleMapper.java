package io.niceday.bali.access.role.mapper;

import io.niceday.bali.access.role.entity.Role;
import io.niceday.bali.access.role.form.RoleForm.Request;
import io.niceday.bali.access.role.form.RoleForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role mapper
 **********************************************************************************************************************/
@Mapper
public interface RoleMapper {

    RoleMapper mapper = Mappers.getMapper(RoleMapper.class);

    Role toRole(Request.Add    form);
    Role toRole(Request.Modify form, Long id);

    Response.FindOne toFindOne(Role entity);
    Response.FindAll toFindAll(Role entity);

    @Mapping(target="id", ignore=true)
    Role modify(Role source, @MappingTarget Role target);
}