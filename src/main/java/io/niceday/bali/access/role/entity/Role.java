package io.niceday.bali.access.role.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.bali.access.role.enumerate.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("권한")
public class Role extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("권한일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("권한구분")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private RoleType roleType;

    @Description("설명")
    @Column(nullable=false, length=500)
    private String description;
}
