package io.niceday.bali.access.role.form;

import io.swagger.annotations.ApiModelProperty;
import io.niceday.common.base.form.BaseForm;
import io.niceday.bali.access.role.enumerate.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role form
 **********************************************************************************************************************/
public class RoleForm {

	public static class Request {

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Find {

            @ApiModelProperty(value="이름")
            private RoleType roleType;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Add {

            @ApiModelProperty(value="권한구분", required=true)
            @NotNull
            private RoleType roleType;

            @ApiModelProperty(value="설명", required=true)
            @Length(max=500)
            @NotEmpty
            private String description;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Modify {

            @ApiModelProperty(value="권한구분", required=true)
            @NotNull
            private RoleType roleType;

            @ApiModelProperty(value="설명", required=true)
            @Length(max=500)
            @NotEmpty
            private String description;
		}
	}
	
	public static class Response {
		
		@Data
		public static class FindAll {

            @ApiModelProperty(value="권한일련번호")
            private Long id;

            @ApiModelProperty(value="권한구분")
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            private String description;
            
            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="수정일시")
            private LocalDateTime updatedAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;

            @ApiModelProperty(value="수정자")
            private BaseForm.Response.Account updator;
		}
		
		@Data
		public static class FindOne {

            @ApiModelProperty(value="권한일련번호")
            private Long id;

            @ApiModelProperty(value="권한구분")
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            private String description;
            
            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="수정일시")
            private LocalDateTime updatedAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;

            @ApiModelProperty(value="수정자")
            private BaseForm.Response.Account updator;
		}
	}
}