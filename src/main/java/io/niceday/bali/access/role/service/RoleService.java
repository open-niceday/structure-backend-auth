package io.niceday.bali.access.role.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.bali.access.role.entity.Role;
import io.niceday.bali.access.role.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.bali.access.role.mapper.RoleMapper.mapper;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class RoleService {

	private final RoleRepository roleRepository;

	@Transactional(readOnly=true)
	public Page<Role> getPage(Predicate search, Pageable pageable) {
		return roleRepository.findAll(search, pageable);
	}

	@Transactional(readOnly=true)
	public Role get(Long roleId) {
		return roleRepository.findById(roleId).orElseThrow(NotFoundException::new);
	}

	public Role add(Role role) {
		return roleRepository.save(role);
	}

	public Role modify(Long roleId, Role role) {
		return mapper.modify(role, get(roleId));
	}

	public void remove(Long roleId) {
		roleRepository.delete(get(roleId));
	}
}
