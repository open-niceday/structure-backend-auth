package io.niceday.bali.access.role.controller;

import io.niceday.bali.access.role.form.RoleForm.Request;
import io.niceday.bali.access.role.form.RoleForm.Response;
import io.niceday.bali.access.role.predicate.RolePredicate;
import io.niceday.bali.access.role.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.bali.access.role.mapper.RoleMapper.mapper;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description role controller
 **********************************************************************************************************************/
@Api(description="권한")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class RoleController {

	private final RoleService roleService;

	@ApiOperation("페이지")
    @GetMapping("/roles/pages")
    public Page<Response.FindAll> getPage(@Valid Request.Find find, @PageableDefault Pageable pageable){
        return roleService.getPage(RolePredicate.search(find), pageable).map(mapper::toFindAll);
    }

    @ApiOperation("조회")
	@GetMapping("/roles/{roleId}")
	public Response.FindOne get(@PathVariable Long roleId){
		return mapper.toFindOne(roleService.get(roleId));
	}

	@ApiOperation("등록")
	@PostMapping("/roles")
	public Response.FindOne add(@Valid @RequestBody Request.Add add){
		return mapper.toFindOne(roleService.add(mapper.toRole(add)));
	}

	@ApiOperation("수정")
	@PutMapping("/roles/{roleId}")
	public Response.FindOne modify(@PathVariable Long roleId, @Valid @RequestBody Request.Modify modify){
		return mapper.toFindOne(roleService.modify(roleId, mapper.toRole(modify, roleId)));
	}

	@ApiOperation("삭제")
	@DeleteMapping("/roles/{roleId}")
	public void remove(@PathVariable Long roleId){
		roleService.remove(roleId);
	}
}