package io.niceday.bali.access.resource.controller;

import io.niceday.bali.access.resource.form.ResourceForm.Request;
import io.niceday.bali.access.resource.form.ResourceForm.Response;
import io.niceday.bali.access.resource.predicate.ResourcePredicate;
import io.niceday.bali.access.resource.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.bali.access.resource.mapper.ResourceMapper.mapper;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource controller
 **********************************************************************************************************************/
@Api(description="자원")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class ResourceController {

	private final ResourceService resourceService;

    @ApiOperation("페이지")
    @GetMapping("/resources/pages")
    public Page<Response.FindAll> getPage(@Valid Request.Find find, @PageableDefault Pageable pageable){
        return resourceService.getPage(ResourcePredicate.search(find), pageable).map(mapper::toFindAll);
    }

    @ApiOperation("조회")
	@GetMapping("/resources/{resourceId}")
	public Response.FindOne get(@PathVariable Long resourceId){
		return mapper.toFindOne(resourceService.get(resourceId));
	}

	@ApiOperation("등록")
	@PostMapping("/resources")
	public Response.FindOne add(@Valid @RequestBody Request.Add add){
		return mapper.toFindOne(resourceService.add(mapper.toResource(add)));
	}

	@ApiOperation("수정")
	@PutMapping("/resources/{resourceId}")
	public Response.FindOne modify(@PathVariable Long resourceId, @Valid @RequestBody Request.Modify modify){
		return mapper.toFindOne(resourceService.modify(resourceId, mapper.toResource(modify, resourceId)));
	}

	@ApiOperation("삭제")
	@DeleteMapping("/resources/{resourceId}")
	public void remove(@PathVariable Long resourceId){
		resourceService.remove(resourceId);
	}
}