package io.niceday.bali.access.resource.repository;

import io.niceday.bali.access.resource.entity.Resource;
import io.niceday.bali.access.resource.enumerate.ScopeType;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource repository
 **********************************************************************************************************************/
@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long>, QuerydslPredicateExecutor<Resource> {

    @EntityGraph(attributePaths="roles", type=EntityGraph.EntityGraphType.FETCH)
    Optional<List<Resource>> findAllByScopeType(ScopeType scopeType);
}