package io.niceday.bali.access.resource.form;

import io.swagger.annotations.ApiModelProperty;
import io.niceday.common.base.form.BaseForm;
import io.niceday.bali.access.resource.enumerate.MethodType;
import io.niceday.bali.access.resource.enumerate.PermissionType;
import io.niceday.bali.access.resource.enumerate.ScopeType;
import io.niceday.bali.access.role.enumerate.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource form
 **********************************************************************************************************************/
public class ResourceForm {

	public static class Request {

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Find {

            @ApiModelProperty(value="이름")
            private String name;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Add {

            @ApiModelProperty(value="이름", required=true)
            @Length(max=500)
            @NotEmpty
            private String name;

            @ApiModelProperty(value="메소드", required=true)
            @NotNull
            private MethodType methodType;

            @ApiModelProperty(value="범위", required=true)
            @NotNull
            private ScopeType scopeType;

            @ApiModelProperty(value="허용", required=true)
            @NotNull
            private PermissionType permissionType;

            @ApiModelProperty(value="설명")
            @Length(max=500)
            private String description;

            @ApiModelProperty("권한목록")
            @Valid
            private List<Role> roles;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Modify {

            @ApiModelProperty(value="이름", required=true)
            @Length(max=500)
            @NotEmpty
            private String name;

            @ApiModelProperty(value="메소드", required=true)
            @NotNull
            private MethodType methodType;

            @ApiModelProperty(value="범위", required=true)
            @NotNull
            private ScopeType scopeType;

            @ApiModelProperty(value="허용",required=true)
            @NotNull
            private PermissionType permissionType;

            @ApiModelProperty(value="설명")
            @Length(max=500)
            private String description;

            @ApiModelProperty("권한목록")
            @Valid
            private List<Role> roles;
		}

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Role {

            @ApiModelProperty(value="권일련번호", required=true)
            @NotNull
            private Long id;
        }
	}
	
	public static class Response {
		
		@Data
		public static class FindAll {

            @ApiModelProperty(value="자원일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="메소드")
            private MethodType methodType;

            @ApiModelProperty(value="범위")
            private ScopeType scopeType;

            @ApiModelProperty(value="허용")
            private PermissionType permissionType;

            @ApiModelProperty(value="설명")
            private String description;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
		}
		
		@Data
		public static class FindOne {

            @ApiModelProperty(value="자원일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="메소드")
            private MethodType methodType;

            @ApiModelProperty(value="범위")
            private ScopeType scopeType;

            @ApiModelProperty(value="허용")
            private PermissionType permissionType;

            @ApiModelProperty(value="설명")
            private String description;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;

            @ApiModelProperty(value="권한목록")
            private List<Role> roles;
		}

        @Data
        public static class Role {

            @ApiModelProperty(value="권한일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            private String description;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
	}
}