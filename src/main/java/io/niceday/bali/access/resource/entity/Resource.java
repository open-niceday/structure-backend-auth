package io.niceday.bali.access.resource.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.bali.access.resource.enumerate.MethodType;
import io.niceday.bali.access.resource.enumerate.PermissionType;
import io.niceday.bali.access.resource.enumerate.ScopeType;
import io.niceday.bali.access.role.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description resource
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Table(name="resource_tic_auth")
@Description("자원")
public class Resource extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("자원일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("이름")
    @Column(nullable=false, length=500)
    private String name;

    @Description("메소드")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private MethodType methodType;

    @Description("범위")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private ScopeType scopeType;

    @Description("허용")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private PermissionType permissionType;

    @Description("설명")
    @Column(length=500)
    private String description;


    @Description("권한목록")
    @OneToMany(fetch= FetchType.LAZY)
    @JoinTable(name="resource_tic_auth_role", joinColumns=@JoinColumn(name="resource_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles;
}