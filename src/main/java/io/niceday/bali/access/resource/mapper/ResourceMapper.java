package io.niceday.bali.access.resource.mapper;

import io.niceday.bali.access.resource.entity.Resource;
import io.niceday.bali.access.resource.form.ResourceForm.Request;
import io.niceday.bali.access.resource.form.ResourceForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource mapper
 **********************************************************************************************************************/
@Mapper
public interface ResourceMapper {

    ResourceMapper mapper = Mappers.getMapper(ResourceMapper.class);

    Resource toResource(Request.Add form);
    Resource toResource(Request.Modify form, Long id);

    Response.FindOne toFindOne(Resource entity);
    Response.FindAll toFindAll(Resource entity);

    @Mapping(target="id", ignore=true)
    Resource modify(Resource source, @MappingTarget Resource target);
}