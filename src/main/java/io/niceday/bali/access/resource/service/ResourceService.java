package io.niceday.bali.access.resource.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.bali.access.resource.entity.Resource;
import io.niceday.bali.access.resource.repository.ResourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.bali.access.resource.mapper.ResourceMapper.mapper;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class ResourceService {

	private final ResourceRepository resourceRepository;

	@Transactional(readOnly=true)
	public Page<Resource> getPage(Predicate search, Pageable pageable) {
		return resourceRepository.findAll(search, pageable);
	}

	@Transactional(readOnly=true)
	public Resource get(Long resourceId) {
		return resourceRepository.findById(resourceId).orElseThrow(NotFoundException::new);
	}

	public Resource add(Resource resource) {
		return resourceRepository.save(resource);
	}

	public Resource modify(Long resourceId, Resource resource) {
		return mapper.modify(resource, get(resourceId));
	}

	public void remove(Long resourceId) {
		resourceRepository.delete(get(resourceId));
	}
}