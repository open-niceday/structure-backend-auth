package io.niceday.bali.access.resource.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description 요청 방법
 *               - GET    : 조회
 *               - POST   : 등록
 *               - PUT    : 수정
 *               - PATCH  : 속성수정
 *               - DELETE : 삭제
 *               - HEAD   : 존재
 *               - NONE   : 없음
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum MethodType {

     GET   ("조회")
    ,POST  ("등록")
    ,PUT   ("수정")
    ,PATCH ("속성수정")
    ,DELETE("삭제")
    ,HEAD  ("여부")
    ,NONE  ("없음");

    private String description;
}