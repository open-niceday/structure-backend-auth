package io.niceday.bali.access.resource.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description 범위
 *               - ALL    : 전체 메소드
 *               - METHOD : 특정 메소드
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum ScopeType {

     ALL   ("전체메소드")
    ,METHOD("특정메소드");

    private String description;
}
