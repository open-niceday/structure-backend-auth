package io.niceday.bali.access.resource.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.bali.access.resource.entity.QResource;
import io.niceday.bali.access.resource.form.ResourceForm.Request;

import java.util.Optional;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description resource predicate
 **********************************************************************************************************************/
public class ResourcePredicate {
	
	public static Predicate search(Request.Find find) {

		QResource 	   resource = QResource.resource;
		BooleanBuilder builder  = new BooleanBuilder();

        Optional.ofNullable(find.getName()).ifPresent(p -> builder.and(resource.name.startsWith(p)));
		return builder;
	}
}