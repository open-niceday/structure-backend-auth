package io.niceday.bali.access.account.controller;

import io.niceday.bali.access.account.predicate.AccountPredicate;
import io.niceday.bali.access.account.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.bali.access.account.form.AccountForm.Request;
import static io.niceday.bali.access.account.form.AccountForm.Response;
import static io.niceday.bali.access.account.mapper.AccountMapper.mapper;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account controller
 **********************************************************************************************************************/
@Api(description="사용자")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class AccountController {

	private final AccountService accountService;

	@ApiOperation("목록")
	@GetMapping("/accounts/pages")
	public Page<Response.FindAll> getPage(@Valid Request.Find find, @PageableDefault Pageable pageable){
		return accountService.getPage(AccountPredicate.search(find), pageable).map(mapper::toFindAll);
	}

	@ApiOperation("조회")
	@GetMapping("/accounts/{accountId}")
	public Response.FindOne get(@PathVariable Long accountId){
		return mapper.toFindOne(accountService.get(accountId));
	}

	@ApiOperation("등록")
	@PostMapping("/accounts")
	public Response.FindOne add(@Valid @RequestBody Request.Add add){
		return mapper.toFindOne(mapper.toAccount(add));
	}

	@ApiOperation("수정-닉네임,프로필")
	@PutMapping("/accounts/{accountId}")
	public Response.FindOne modify(@PathVariable Long accountId, @Valid @RequestBody Request.Modify modify){
		return mapper.toFindOne(mapper.toAccount(modify, accountId));
	}

	@Deprecated
	@ApiOperation("삭제")
	@DeleteMapping("/accounts/{accountId}")
	public void remove(@PathVariable Long accountId){
		accountService.remove(accountId);
	}
}
