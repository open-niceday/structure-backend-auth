package io.niceday.bali.access.account.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.bali.access.account.entity.Account;
import io.niceday.bali.access.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static io.niceday.bali.access.account.mapper.AccountMapper.mapper;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class AccountService {

	private final AccountRepository accountRepository;

	@Transactional(readOnly=true)
	public Page<Account> getPage(Predicate predicate, Pageable pageable) {
		return accountRepository.findAll(predicate, pageable);
	}

	@Transactional(readOnly=true)
	public Account get(Long accountId) {
		return accountRepository.findById(accountId).orElseThrow(NotFoundException::new);
	}

	public Optional<Account> getByEmail(String email) {
		return accountRepository.findByEmail(email);
	}

	public Account add(Account account) {
		return accountRepository.save(account);
	}

	public Account modify(Long accountId, Account account) {
		return mapper.modify(account, get(accountId));
	}

	public void remove(Long accountId) {
		accountRepository.delete(get(accountId));
	}
}
