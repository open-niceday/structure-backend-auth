package io.niceday.bali.access.account.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.bali.access.account.entity.QAccount;
import org.apache.commons.lang3.StringUtils;
import static io.niceday.bali.access.account.form.AccountForm.Request;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account predicate
 **********************************************************************************************************************/
public class AccountPredicate {

	public static Predicate search(Request.Find find) {

		QAccount 	   account = QAccount.account;
		BooleanBuilder builder = new BooleanBuilder();

		if(StringUtils.isNotBlank(find.getName())){
			builder.and(account.name.startsWith(find.getName()));
		}

		if(StringUtils.isNotBlank(find.getEmail())){
			builder.and(account.email.startsWith(find.getEmail()));
		}

		return builder;
	}
}