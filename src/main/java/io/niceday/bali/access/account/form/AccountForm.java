package io.niceday.bali.access.account.form;

import io.swagger.annotations.ApiModelProperty;
import io.niceday.bali.access.account.enumerate.AccountStatus;
import io.niceday.bali.access.role.enumerate.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account form
 **********************************************************************************************************************/
public class AccountForm {

	public static class Request {

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Find {

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="이메일")
			private String email;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Add {

			@ApiModelProperty(value="이름", required=true)
			@NotBlank
			@Length(max=100)
			private String name;

			@ApiModelProperty(value="이메일", required=true)
			@NotBlank
			@Length(max=100)
			private String email;
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Modify {

			@ApiModelProperty(value="이름", required=true)
			@NotBlank
			@Length(max=100)
			private String name;

			@ApiModelProperty(value="이메일", required=true)
			@NotBlank
			@Length(max=100)
			private String email;
		}
	}

	public static class Response {

		@Data
		public static class FindAll {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="사용자상태")
			private AccountStatus accountStatus;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="이메일")
			private String email;

            @ApiModelProperty(value="권한목록")
			private List<Role> roles;
		}

		@Data
		public static class FindOne {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="사용자상태")
			private AccountStatus accountStatus;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="이메일")
			private String email;

			@ApiModelProperty(value="권한목록")
			private List<Role> roles;
		}

		@Data
		public static class Role {

			@ApiModelProperty(value="권한일련번호")
			private Long id;

			@ApiModelProperty(value="이름")
			private RoleType roleType;

			@ApiModelProperty(value="설명")
			private String description;
		}
	}
}
