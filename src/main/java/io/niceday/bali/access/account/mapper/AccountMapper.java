package io.niceday.bali.access.account.mapper;

import io.niceday.bali.access.account.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import static io.niceday.bali.access.account.form.AccountForm.Request;
import static io.niceday.bali.access.account.form.AccountForm.Response;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account mapper
 **********************************************************************************************************************/
@Mapper
public interface AccountMapper {

    AccountMapper mapper = Mappers.getMapper(AccountMapper.class);

    Account toAccount(Request.Add    form);
    Account toAccount(Request.Modify form, Long id);

    Response.FindOne toFindOne(Account entity);
    Response.FindAll toFindAll(Account entity);

    @Mapping(target="id", ignore=true)
    Account modify(Account source, @MappingTarget Account target);
}