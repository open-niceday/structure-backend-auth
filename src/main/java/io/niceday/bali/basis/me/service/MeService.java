package io.niceday.bali.basis.me.service;

import io.niceday.common.security.principal.helper.PrincipalHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.bali.access.account.form.AccountForm.Response;
import static io.niceday.bali.access.account.mapper.AccountMapper.mapper;

/**
 * @since       2021.04.06
 * @author      lucas
 * @description me service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class MeService {

	@Transactional(readOnly=true)
	public Response.FindOne get() {
		return mapper.toFindOne(PrincipalHelper.getAccount());
	}
}