package io.niceday.bali.basis.me.controller;

import io.niceday.bali.basis.me.service.MeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static io.niceday.bali.access.account.form.AccountForm.Response;
/**
 * @since       2021.04.06
 * @author      lucas
 * @description me controller
 **********************************************************************************************************************/
@Api(description="토큰")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class MeController {

	private final MeService meService;

	@ApiOperation("조회")
	@GetMapping("/me")
	public Response.FindOne get(){
		return meService.get();
	}
}
