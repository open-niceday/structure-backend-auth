package io.niceday.bali.common.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description error form
 **********************************************************************************************************************/
public class ErrorForm {

    @Data
    public static class Error400 {

        @ApiModelProperty(value="에러코드", example="E00010000")
        private String code;

        @ApiModelProperty(value="에러메세지", example="유효성 검사에 실패하였습니다.")
        private String message;

        @ApiModelProperty(value="속성별에러")
        private List<Error> errors;

        @Data
        public static class Error {

            @ApiModelProperty(value="속성명", example="content")
            private String       field;

            @ApiModelProperty(value="에러메세지", example="공백일 수 없습니다.")
            private List<String> messages;
        }
    }

    @Data
    public static class Error401 {

        @ApiModelProperty(value="에러코드", example="access_denied")
        private String error;

        @ApiModelProperty(value="에러메세지", example="Access is denied")
        private String error_description;
    }

    @Data
    public static class Error403 {


        @ApiModelProperty(value="에러코드", example="unauthorized")
        private String error;

        @ApiModelProperty(value="에러메세지", example="Full authentication is required to access this resource")
        private String error_description;
    }

    @Data
    public static class Error404 {

        @ApiModelProperty(value="에러코드", example="E00010000")
        private String code;

        @ApiModelProperty(value="에러메세지", example="오류가 발생했습니다.")
        private String message;
    }

    @Data
    public static class Error406 {

        @ApiModelProperty(value="에러코드", example="E00010000")
        private String code;

        @ApiModelProperty(value="에러메세지", example="오류가 발생했습니다.")
        private String message;
    }

    @Data
    public static class Error503 {

        @ApiModelProperty(value="에러코드", example="E00010000")
        private String code;

        @ApiModelProperty(value="에러메세지", example="오류가 발생했습니다.")
        private String message;
    }
}