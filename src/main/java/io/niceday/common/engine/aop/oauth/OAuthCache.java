package io.niceday.common.engine.aop.oauth;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @since       2021.09.15
 * @author      lucas
 * @description oauth cache
 **********************************************************************************************************************/
@Service
public class OAuthCache {

    @Cacheable(cacheNames="get-load-client-by-client-id", key="'get-load-client-by-client-id-' + #clientId")
    public Object loadClientByClientId(ProceedingJoinPoint point, String clientId) throws Throwable {
        return point.proceed(point.getArgs());
    }

    @Cacheable(cacheNames="get-read-access-token-by-token", key="'get-read-access-token-by-token-' + #token")
    public Object readAccessToken(ProceedingJoinPoint point, String token) throws Throwable {
        return point.proceed(point.getArgs());
    }

    @Cacheable(cacheNames="get-read-authentication-by-token", key="'get-read-authentication-by-token-' + #token")
    public Object readAuthentication(ProceedingJoinPoint point, String token) throws Throwable {
        return point.proceed(point.getArgs());
    }

    @CacheEvict(cacheNames="get-read-access-token-by-token", key="'get-read-access-token-by-token-' + #token")
    public Object removeAccessToken(ProceedingJoinPoint point, String token) throws Throwable {
        return point.proceed(point.getArgs());
    }
}
