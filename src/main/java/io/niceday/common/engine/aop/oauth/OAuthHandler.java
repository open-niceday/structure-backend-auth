package io.niceday.common.engine.aop.oauth;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description oauth handler
 **********************************************************************************************************************/
@Aspect
@Component
@RequiredArgsConstructor
public class OAuthHandler {

    private final OAuthCache oAuthCache;

    @Around("execution(* org.springframework.security.oauth2.provider.client.JdbcClientDetailsService.loadClientByClientId(..)) && args(String) && args(clientId)")
    public Object loadClientByClientId(ProceedingJoinPoint point, String clientId) throws Throwable {
        return oAuthCache.loadClientByClientId(point, clientId);
    }

    @Around("execution(* org.springframework.security.oauth2.provider.token.TokenStore.readAccessToken(..)) && args(String) && args(token)")
    public Object readAccessToken(ProceedingJoinPoint point, String token) throws Throwable {
        return oAuthCache.readAccessToken(point, token);
    }

    @Around("execution(* org.springframework.security.oauth2.provider.token.TokenStore.readAuthentication(..)) && args(String) && args(token)")
    public Object readAuthenticationByString(ProceedingJoinPoint point, String token) throws Throwable {
        return oAuthCache.readAuthentication(point, token);
    }

    @Around("execution(* org.springframework.security.oauth2.provider.token.TokenStore.readAuthentication(..)) && args(org.springframework.security.oauth2.common.OAuth2AccessToken) && args(token)")
    public Object readAuthenticationByOAuth2AccessToken(ProceedingJoinPoint point, OAuth2AccessToken token) throws Throwable {
        return oAuthCache.readAuthentication(point, token.getValue());
    }

    @Around("execution(* org.springframework.security.oauth2.provider.token.TokenStore.removeAccessToken(..)) && args(org.springframework.security.oauth2.common.OAuth2AccessToken) && args(token)")
    public Object removeAccessTokenByOAuth2AccessToken(ProceedingJoinPoint point, OAuth2AccessToken token) throws Throwable {
        return oAuthCache.removeAccessToken(point, token.getValue());
    }
}