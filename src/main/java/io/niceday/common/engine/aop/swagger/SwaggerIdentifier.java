package io.niceday.common.engine.aop.swagger;

import com.google.common.collect.ImmutableList;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.niceday.common.engine.annotation.controller.Swagger.ApiResponseMessage;
import io.niceday.common.engine.annotation.controller.Swagger.ApiResponseModel;
import io.niceday.common.engine.constant.Constant;
import io.niceday.common.engine.helper.message.MessageHelper;
import lombok.SneakyThrows;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.util.CastUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.Model;
import springfox.documentation.schema.ModelProperty;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Operation;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.service.contexts.RequestMappingContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;

/**
 * @since       2021.10.28
 * @author      preah
 * @description api response message identifier
 **********************************************************************************************************************/
@Aspect
@Component
public class SwaggerIdentifier {

    private final String        NEW_LINE = "\t\n";
    private final List<String>  ATTRIBUTE_EXCLUDE_KEY = ImmutableList.of("payload", "groups", "message");
    private final List<Package> VALIDATION_PACKAGES   = ImmutableList.of(Package.getPackage("javax.validation.constraints"), Package.getPackage("org.hibernate.validator.constraints"));

    private RequestMappingContext context = null;

    @AfterReturning(value="execution(* springfox.documentation.spring.web.readers.operation.ApiOperationReader.read(..))", returning="operations")
    public void addResponseMessage(JoinPoint joinPoint, List<Operation> operations) {

        for(Object arg : joinPoint.getArgs()) {
            if(arg instanceof RequestMappingContext) {
                context = CastUtils.cast(arg);
            }
        }

        for(Operation operation : operations) {

            operation.getParameters()
                     .stream ()
                     .filter (parameter -> parameter.getType().get().getErasedType().isEnum())
                     .forEach(parameter -> setField(parameter, "description", StringUtils.join(parameter.getDescription()
                                                                                                   , NEW_LINE
                                                                                                   , this.getEnumValue(parameter.getType().get().getErasedType()))));

            Set<ResponseMessage> responseMessage = operation.getResponseMessages();
            this.addResponseMessage(responseMessage);
            this.addResponseModel  (responseMessage);
        }
    }

    private void addResponseModel(Set<ResponseMessage> responseMessage) {

        com.google.common.base.Optional<ApiResponseModel>       single = context.findAnnotation(ApiResponseModel.class);
        com.google.common.base.Optional<ApiResponseModel.Group> multi  = context.findAnnotation(ApiResponseModel.Group.class);

        if(single.isPresent()) {
            responseMessage.add(this.toMessage(single.get()));
        }

        if(multi.isPresent()) {
            for(ApiResponseModel message : multi.get().value()) {
                responseMessage.add(this.toMessage(message));
            }
        }
    }

    private void addResponseMessage(Set<ResponseMessage> responseMessage) {

        com.google.common.base.Optional<ApiResponseMessage>       single = context.findAnnotation(ApiResponseMessage.class);
        com.google.common.base.Optional<ApiResponseMessage.Group> multi  = context.findAnnotation(ApiResponseMessage.Group.class);

        if(single.isPresent()) {
            responseMessage.add(this.toMessage(single.get()));
        }

        if(multi.isPresent()) {
            for(ApiResponseMessage message : multi.get().value()) {
                responseMessage.add(this.toMessage(message));
            }
        }
    }

    @AfterReturning(value="execution(* springfox.documentation.swagger2.mappers.ServiceModelToSwagger2Mapper.mapDocumentation(..))", returning="swagger")
    public void afterResponseMessage(JoinPoint joinPoint, Swagger swagger) {

        for(String pathKey : swagger.getPaths().keySet()) {
            Path path = swagger.getPath(pathKey);

            path.getOperations().stream().findAny().ifPresent(operation -> {

                Map<String, Response> map = new HashMap();
                operation.getResponses().entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(entry -> {

                    String[] description = entry.getValue().getDescription().split(StringUtils.join(Constant.String.BACKSLASH, Constant.String.PIPE));
                    entry.getValue().setDescription(description.length <= Constant.Integer.ONE ? entry.getValue().getDescription() : description[Constant.Integer.ONE]);
                    map.put(description.length <= Constant.Integer.ONE ? entry.getKey() : description[Constant.Integer.ZERO], entry.getValue());
                });
                operation.setResponses(map);
            });
        }
    }

    @SneakyThrows
    private String getEnumValue(Class<?> clazz) {
       return Objects.toString(Arrays.stream (clazz.getEnumConstants())
                                     .map    (target -> {
                                         if(ObjectUtils.allNotNull(target, getFieldValue(target, "description"))) {
                                             return StringUtils.join(Objects.toString(target)
                                                                   , Constant.String.COLON
                                                                   , Objects.toString(this.getFieldValue(target, "description")));
                                         }
                                         return Constant.String.EMPTY;
                                     }).collect(Collectors.toList()));
    }

    @SneakyThrows
    @AfterReturning(value="execution(* springfox.documentation.spring.web.scanners.ApiModelReader.read(..))", returning="modelMap")
    public void afterModel(JoinPoint joinPoint, Map<String, Model> modelMap) {

        for(String modelKey : modelMap.keySet()) {
            Model model = modelMap.get(modelKey);

            for(String propertyKey : model.getProperties().keySet()) {
                ModelProperty property = model.getProperties().get(propertyKey);

                Optional.ofNullable(this.getField(Class.forName(model.getQualifiedType()), propertyKey))
                        .ifPresent(field -> {

                            if(field.getType().isEnum()) {
                                if(BooleanUtils.isFalse(StringUtils.contains(property.getDescription(), Constant.String.COLON))){
                                    setField(property, "description", StringUtils.join(property.getDescription(), Constant.String.BLANK, this.getEnumValue(property.getType().getErasedType())));
                                }
                            }

                            Optional.ofNullable(this.findAnnotation(field))
                                    .ifPresent(annotations -> {
                                        if(annotations.stream().noneMatch(annotation -> property.getDescription().contains(annotation))) {
                                            setField(property, "description", StringUtils.join(property.getDescription(), NEW_LINE, Objects.toString(annotations)));
                                        }
                            });
                });
            }
        }
    }

    private List<String> findAnnotation(Field field){

        return Arrays.stream(field.getDeclaredAnnotations())
                     .filter(annotation -> VALIDATION_PACKAGES.stream().anyMatch(location -> annotation.annotationType().getPackage().equals(location)))
                     .sorted(Comparator.comparing(Annotation::toString))
                     .map   (annotation -> {
                              Map<String, Object> attribute = AnnotationUtils.getAnnotationAttributes(annotation);
                              return StringUtils.join(annotation.annotationType().getSimpleName()
                                                    , attribute.entrySet().stream()
                                                                          .filter (entry -> ATTRIBUTE_EXCLUDE_KEY.stream().noneMatch(key -> key.equals(entry.getKey())))
                                                                          .collect(collectingAndThen(Collectors.toList(), entries -> CollectionUtils.isNotEmpty(entries) ? Objects.toString(entries) : Constant.String.EMPTY)));
                     }).collect(collectingAndThen(Collectors.toList(), list -> list.isEmpty() ? null : list));
    }

    private ResponseMessage toMessage(ApiResponseModel model) {

        return new ResponseMessageBuilder()
                .code         (RandomUtils.nextInt())
                .message      (StringUtils.join(model.condition(), Constant.String.PIPE, model.description()))
                .responseModel(new ModelRef(model.model().getTypeName()))
                .build();
    }

    private ResponseMessage toMessage(ApiResponseMessage message) {

        return new ResponseMessageBuilder()
                .code   (message.code().value())
                .message(Arrays.stream (message.exception())
                        .map    (exceptionCode -> StringUtils.join("[", exceptionCode.name(), "]", MessageHelper.getMessage(exceptionCode)))
                        .collect(Collectors.joining(NEW_LINE)))
                .build();
    }

    @SneakyThrows
    private void setField(Object target, String name, String value) {
        Field field = target.getClass().getDeclaredField(name);
        if(ObjectUtils.allNotNull(field)) {
            ReflectionUtils.makeAccessible(field);
            field.set(target, value);
        }
    }

    private Object getFieldValue(Object target, String name) {
        try {
            Field field = target.getClass().getDeclaredField(name);
            if(ObjectUtils.allNotNull(field)) {
                ReflectionUtils.makeAccessible(field);
                return ReflectionUtils.getField(field, target);
            }
        } catch (Exception e) {return null;}
        return null;
    }

    private Field getField(Class target, String name) {
        try {
            Field field = target.getDeclaredField(name);
            if(ObjectUtils.allNotNull(field)) {
                ReflectionUtils.makeAccessible(field);
                return field;
            }
        } catch (Exception e) {return null;}
        return null;
    }
}
