package io.niceday.common.engine.aop.swagger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description model identifier
 **********************************************************************************************************************/
@Aspect
@Component
public class ModelIdentifier {

    @Around("execution(* springfox.documentation.schema.DefaultTypeNameProvider.nameFor(..))")
    public Object doSomethingAround(ProceedingJoinPoint point) throws Throwable {

        Class argument = (Class)point.getArgs()[0];
        point.proceed();
        return argument.getName();
    }
}
