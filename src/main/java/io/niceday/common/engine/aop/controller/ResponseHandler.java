package io.niceday.common.engine.aop.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;

/**  
 * @since       2021.02.01
 * @author      lucas
 * @description response handler
 **********************************************************************************************************************/
@ControllerAdvice(annotations={ResponseMapper.class})
@Deprecated
public class ResponseHandler {
    
}

