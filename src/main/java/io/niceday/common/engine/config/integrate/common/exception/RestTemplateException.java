package io.niceday.common.engine.config.integrate.common.exception;

import io.niceday.common.engine.exception.common.ExceptionCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @since       2020.04.01
 * @author      lucas
 * @description rest template exception
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class RestTemplateException extends RuntimeException {

	public RestTemplateException(){
		super(ExceptionCode.E00100100.name());
	}

	public RestTemplateException(ExceptionCode exceptionCode, HttpStatus httpStatus){
		super(exceptionCode.name());
		code   = exceptionCode;
		status = httpStatus;
	}

	public RestTemplateException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}

	@Getter
	public ExceptionCode code   = ExceptionCode.E00100100;
	public HttpStatus    status = HttpStatus.BAD_REQUEST;
}