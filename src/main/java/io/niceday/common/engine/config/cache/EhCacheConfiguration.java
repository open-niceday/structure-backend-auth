package io.niceday.common.engine.config.cache;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @since       2020.04.06
 * @author      lucas
 * @description ehcache configuration
 **********************************************************************************************************************/
@Configuration
@EnableCaching
public class EhCacheConfiguration {
 
}