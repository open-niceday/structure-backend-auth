package io.niceday.common.engine.config.swagger;

import java.util.Collections;

import com.google.common.collect.ImmutableList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import io.niceday.common.engine.helper.property.PropertyHelper;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description swagger configuration
 **********************************************************************************************************************/
@Configuration
@EnableSwagger2
@Profile({"default", "test", "stage", "dev"})
public class SwaggerConfiguration implements WebMvcConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/**")
				.addResourceLocations("classpath:/config/swagger/");

		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Bean
	public Docket swaggerDefault() {

		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("bali")
				.select()
				.apis(RequestHandlerSelectors.basePackage("io.niceday.bali"))
				.paths(PathSelectors.ant("/api/**"))
				.build()
				.securitySchemes(ImmutableList.of(new ApiKey("authorization", "authorization", "header")))
				.useDefaultResponseMessages(false)
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {

		return new ApiInfo(
				 PropertyHelper.getProperty().getSwagger().getInfo().getTitle()
				,PropertyHelper.getProperty().getSwagger().getInfo().getDesc()
				,PropertyHelper.getProperty().getSwagger().getInfo().getVersion()
				,null
				,new Contact(
						 PropertyHelper.getProperty().getSwagger().getContact().getName()
						,PropertyHelper.getProperty().getSwagger().getContact().getUrl()
						,PropertyHelper.getProperty().getSwagger().getContact().getEmail()
				)
				,null
				,null
				,Collections.emptyList()
		);
	}
}
