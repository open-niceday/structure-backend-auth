package io.niceday.common.engine.config.cache;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

/**
 * @since       2020.04.06
 * @author      lucas
 * @description ehcache event listener
 **********************************************************************************************************************/
@Slf4j
public class EhCacheEventListener implements CacheEventListener<Object, Object> {

    @Override
    public void onEvent(CacheEvent cacheEvent) {
        log.debug("[{}-{}]{}-{}", cacheEvent.getType(), cacheEvent.getKey(), cacheEvent.getOldValue(), cacheEvent.getNewValue());
    }
}