package io.niceday.common.engine.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description properties configuration
 **********************************************************************************************************************/
@Data
@Component
@ConfigurationProperties(prefix="security")
public class SecurityConfiguration {

    private Oauth2 oauth2 = null;

    @Data
    public static class Oauth2 {

        private Google      google   = null;
        private Facebook    facebook = null;
        private Apple       apple    = null;
        private Genie       genie    = null;

        @Data
        public static class Google {

            private String uriToken = null;
            private String uriUser  = null;
        }

        @Data
        public static class Facebook {

            private String uriToken = null;
            private String uriUser  = null;
        }

        @Data
        public static class Genie {

            private String uriToken = null;
            private String uriUser  = null;
        }

        @Data
        public static class Apple {

            private String uriKeys = null;
            private String clientIss  = null;
        }
    }

}
