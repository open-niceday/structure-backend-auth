package io.niceday.common.engine.config.schedule;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**   
 * @since       2021.09.16
 * @author      lucas
 * @description schedule configuration
 **********************************************************************************************************************/
@Configuration 
@EnableScheduling
public class ScheduleConfiguration { 
	
}
