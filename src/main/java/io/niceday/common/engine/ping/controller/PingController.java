package io.niceday.common.engine.ping.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**   
 * @since       2021.04.21
 * @author      lucas
 * @description ping controller
 **********************************************************************************************************************/
@Api(description="핑")
@RestController
@RequiredArgsConstructor
public class PingController {

	@ApiOperation("aws health check")
	@GetMapping("/lb_check")
	public boolean eyes(){
		return Boolean.TRUE;
	}
}
