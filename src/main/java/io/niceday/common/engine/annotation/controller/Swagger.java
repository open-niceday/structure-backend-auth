package io.niceday.common.engine.annotation.controller;


import io.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

import java.lang.annotation.*;

/**
 * @since       2021.10.28
 * @author      preah
 * @description swagger ui
 **********************************************************************************************************************/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Swagger {

    @Repeatable(ApiResponseMessage.Group.class)
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ApiResponseMessage {

        HttpStatus code() default HttpStatus.OK;
        ExceptionCode[] exception();

        @Target(ElementType.METHOD)
        @Retention(RetentionPolicy.RUNTIME)
        @interface Group {
            ApiResponseMessage[] value();
        }
    }

    @Repeatable(ApiResponseModel.Group.class)
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ApiResponseModel {

        String condition();
        String description();
        Class  model();

        @Target(ElementType.METHOD)
        @Retention(RetentionPolicy.RUNTIME)
        @interface Group {
            ApiResponseModel[] value();
        }
    }

}
