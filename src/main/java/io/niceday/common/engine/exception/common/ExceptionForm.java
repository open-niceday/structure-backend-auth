package io.niceday.common.engine.exception.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @since       2020.20.24
 * @author      lucas
 * @description exception form
 **********************************************************************************************************************/
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionForm {

    @ApiModelProperty(required=true, value="응답 코드")
	private ExceptionCode code;

    @ApiModelProperty(required=true, value="메시지 내용")
	private String        message;

	public static ExceptionForm create(ExceptionCode code){
		return ExceptionForm.builder()
				.code   (code)
				.message(null)
				.build();
	}

	public static ExceptionForm create(ExceptionCode code, String message){
		return ExceptionForm.builder()
				.code   (code)
				.message(message)
				.build();
	}
}