package io.niceday.common.engine.exception.common;

import lombok.Getter;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description not acceptable exception(요청을 수락 할 수 없는 예외)
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class NotAcceptableException extends RuntimeException {

	@Getter
	public ExceptionCode code;

	public NotAcceptableException(){
		super(ExceptionCode.E00010004.name());
		code = ExceptionCode.E00010004;
	}
	
	public NotAcceptableException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code = exceptionCode;
	}
	
	public NotAcceptableException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}
}
