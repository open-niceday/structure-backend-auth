package io.niceday.common.engine.helper.oauth;

import java.util.Base64;

/**
 * @since       2020.03.11
 * @author      lucas
 * @description oauth helper
 **********************************************************************************************************************/
public class OAuthHelper {

	public static String getBasic(String cliendId, String clientPw){
		String client = String.format("%s:%s", cliendId, clientPw);
		return String.format( "Basic %s", Base64.getEncoder().encodeToString(client.getBytes()));
	}
}
