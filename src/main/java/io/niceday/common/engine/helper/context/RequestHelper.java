package io.niceday.common.engine.helper.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.niceday.common.security.principal.helper.PrincipalHelper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description request helper
 **********************************************************************************************************************/
@Slf4j
@Component
public class RequestHelper {
 
	public static HttpServletRequest getRequest(){
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static String getParameter(String name){
		return getRequest().getParameter(name);
	}

	@SneakyThrows
	public static void currentRequestPrint() {

		HttpServletRequest request   = getRequest();
		String             method    = request.getMethod();
		StringBuffer       url       = request.getRequestURL();
		Long               accountId = PrincipalHelper.getId();

		log.info("method={}, url={}, account={}", method, url, accountId);
	}

    public static HttpServletResponse getResponse(){
        return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
    }
}
