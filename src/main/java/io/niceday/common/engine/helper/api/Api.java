package io.niceday.common.engine.helper.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpMethod;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description api
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Api {

	private Class      clazz;
	private HttpMethod method;
	private String     uri;
	private String     description;
}