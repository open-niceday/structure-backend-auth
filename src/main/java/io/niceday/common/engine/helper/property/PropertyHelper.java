package io.niceday.common.engine.helper.property;

import io.niceday.common.engine.config.properties.PropertiesConfiguration;
import io.niceday.common.engine.config.properties.SecurityConfiguration;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description property helper 
 **********************************************************************************************************************/
@Component
public class PropertyHelper {
	
	@Autowired
    private PropertyHelper(PropertiesConfiguration property, SecurityConfiguration security) {
        PropertyHelper.property = property;
        PropertyHelper.security = security;
    }
	
	@Getter
	private static PropertiesConfiguration  property = null;

	@Getter
	private static SecurityConfiguration    security = null;
}