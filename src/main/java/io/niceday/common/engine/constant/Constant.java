package io.niceday.common.engine.constant;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description constant
 **********************************************************************************************************************/
public class Constant {

    public static class String {
        public final static java.lang.String EMPTY               = "";
        public final static java.lang.String BLANK               = " ";
        public final static java.lang.String ASTERISK            = "*";
        public final static java.lang.String COMMA               = ",";
        public final static java.lang.String COLON               = ":";
        public final static java.lang.String DOT                 = ".";
        public final static java.lang.String SLASH               = "/";
        public final static java.lang.String BACKSLASH           = "\\";
        public final static java.lang.String PIPE                = "|";
    }

    public static class Integer {
        public final static java.lang.Integer ZERO  =  0;
        public final static java.lang.Integer ONE   =  1;
        public final static java.lang.Integer TWO   =  2;
        public final static java.lang.Integer THREE =  3;
        public final static java.lang.Integer FIVE  =  5;
        public final static java.lang.Integer EIGHT = 8;
        public final static java.lang.Integer TEN   = 10;
        public final static java.lang.Integer FIFTEEN = 15;
    }

    public static class Char {
        public final static java.lang.Character A = 'A';
        public final static java.lang.Character B = 'B';
        public final static java.lang.Character C = 'C';
        public final static java.lang.Character D = 'D';
        public final static java.lang.Character Z = 'Z';
    }

    public static class Account {
        public final static java.lang.String DEFAULT_PROFILE_ID = "09665a1c-5072-4a5c-aeeb-b56f2b17b072";
        public final static java.lang.String ANONYMOUS = "ANONYMOUS";
    }

    public static class Regex {
        public final static java.lang.String ONLY_NUMBERS    = "[0-9]+";
        public final static java.lang.String COLOR_CODE      = "^(?:#|0x)?([A-Fa-f\\d]{8}|[A-Fa-f\\d]{6}|[A-Fa-f\\d]{3})$";
    }
}