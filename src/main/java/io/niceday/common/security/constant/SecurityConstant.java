package io.niceday.common.security.constant;

/**
* @since       2021.03.25
* @author      lucas
* @description security constant
**********************************************************************************************************************/
public class SecurityConstant {

    public final static String IS_AUTHENTICATED = "isAuthenticated()";
    public final static String PERMIT_ALL       = "permitAll()";
}
