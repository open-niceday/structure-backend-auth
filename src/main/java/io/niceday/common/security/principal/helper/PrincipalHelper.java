package io.niceday.common.security.principal.helper;

import io.niceday.bali.access.account.entity.Account;
import io.niceday.common.engine.helper.model.ObjectHelper;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static io.niceday.common.security.principal.form.PrincipalForm.Principal;
import static io.niceday.common.security.principal.mapper.PrincipalMapper.mapper;

/**
 * @since       2020.03.06
 * @author      lucas
 * @description principal helper
 *               - add principal in security context : https://www.baeldung.com/manually-set-user-authentication-spring-security
 **********************************************************************************************************************/
@Component
public class PrincipalHelper {

    public static Authentication getAuthentication(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!ObjectUtils.allNotNull(authentication) || BooleanUtils.isFalse(authentication.isAuthenticated()) || authentication instanceof AnonymousAuthenticationToken){
            return null;
        }
        return authentication;
    }

    public static OAuth2AuthenticationDetails getDetails(){
        Authentication authentication = getAuthentication();
        if(Objects.isNull(authentication)){
            return null;
        }

        return OAuth2AuthenticationDetails.class.cast(authentication.getDetails());
    }

    public static Account getAccount(){
        if(Objects.isNull(getAuthentication())){
            return null;
        }
        return mapper.toAccount(ObjectHelper.toInstanceWithDecodeBase64(Principal.class, Objects.toString(getAuthentication().getPrincipal())));
    }

    public static Long getId() {
        return getAccount().getId();
    }
}