package io.niceday.common.security.principal.mapper;

import io.niceday.bali.access.account.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import static io.niceday.common.security.principal.form.PrincipalForm.Principal;

/**
 * @since       2022.11.15
 * @author      lucas
 * @description principal mapper
 **********************************************************************************************************************/
@Mapper
public interface PrincipalMapper {

    PrincipalMapper mapper = Mappers.getMapper(PrincipalMapper.class);

    @Mapping(target="roles", source="entity.roles")
    Principal toPrincipal(Account entity);

    @Mapping(target="roles", source="form.roles")
    Account toAccount(Principal form);
}