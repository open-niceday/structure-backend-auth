package io.niceday.common.security.config;

import io.niceday.bali.access.resource.entity.Resource;
import io.niceday.bali.access.resource.enumerate.ScopeType;
import io.niceday.bali.access.resource.repository.ResourceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @since       2022.11.14
 * @author      lucas
 * @description resource server configuration
 **********************************************************************************************************************/
@Slf4j
@Transactional
@RequiredArgsConstructor
@EnableResourceServer
@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private final ResourceRepository resourceRepository;
    private final TokenStore         tokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        addMatchersByMethod (http.authorizeRequests(), resourceRepository.findAllByScopeType(ScopeType.METHOD).orElse(Collections.emptyList()));
        addMatchersByPattern(http.authorizeRequests(), resourceRepository.findAllByScopeType(ScopeType.ALL   ).orElse(Collections.emptyList()));

        http.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }

    private void addMatchersByMethod(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry, List<Resource> resources){
        for(Resource resource : resources){
            if(ObjectUtils.allNotNull(resource.getPermissionType())){
                ExpressionUrlAuthorizationConfigurer.AuthorizedUrl authorizedUrl = registry.antMatchers(HttpMethod.resolve(resource.getMethodType().name()), resource.getName());
                switch (resource.getPermissionType()) {
                    case    PERMIT : authorizedUrl.permitAll(); break;
                    default        : authorizedUrl.denyAll();   break;
                }
                continue;
            }
            registry.antMatchers    (HttpMethod.resolve(resource.getMethodType().name()), resource.getName())
                    .hasAnyAuthority(Optional.ofNullable(resource.getRoles()).orElse(Collections.emptyList()).stream()
                            .map    (role -> role.getRoleType().name())
                            .toArray(String[]::new));
        }
    }

    private void addMatchersByPattern(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry, List<Resource> resources){
        for(Resource resource : resources) {
            switch (resource.getPermissionType()) {
                case    PERMIT : registry.antMatchers(resource.getName()).permitAll(); break;
                default        : registry.antMatchers(resource.getName()).denyAll();   break;
            }
        }
    }
}
