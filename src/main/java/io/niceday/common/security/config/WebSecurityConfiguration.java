package io.niceday.common.security.config;

import io.niceday.common.security.provider.DefaultAuthenticationProvider;
import io.niceday.common.security.provider.DefaultPreAuthenticatedAuthenticationProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description web security configuration
 **********************************************************************************************************************/
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.BASIC_AUTH_ORDER)
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final DefaultAuthenticationProvider                 defaultAuthenticationProvider;
    private final DefaultPreAuthenticatedAuthenticationProvider defaultPreAuthenticatedAuthenticationProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder
                .authenticationProvider(defaultAuthenticationProvider)
                .authenticationProvider(defaultPreAuthenticatedAuthenticationProvider);
    }
}