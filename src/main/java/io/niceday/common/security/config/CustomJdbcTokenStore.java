package io.niceday.common.security.config;

import java.sql.Types;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Component;

/**
 * @since       2021.09.17
 * @author      lucas
 * @description custom jdbc token store
 *               - add expire_at
 *               - delete expire tokens(access_token, refresh_token) from oauth scheduler
 **********************************************************************************************************************/
@Component
public class CustomJdbcTokenStore extends JdbcTokenStore {

    private static final String INSERT_ACCESS_TOKEN_SQL  = "insert into oauth_access_token  (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token, expire_at) values (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String INSERT_REFRESH_TOKEN_SQL = "insert into oauth_refresh_token (token_id, token, authentication, expire_at) values (?, ?, ?, ?)";

    private final JdbcTemplate               jdbcTemplate;
    private final AuthenticationKeyGenerator authenticationKeyGenerator;

    public CustomJdbcTokenStore(DataSource dataSource) {
        super(dataSource);
        this.jdbcTemplate               = new JdbcTemplate(dataSource);
        this.authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator();
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {

        String refreshToken = null;
        if (token.getRefreshToken() != null) {
            refreshToken = token.getRefreshToken().getValue();
        }

        if (readAccessToken(token.getValue()) != null) {
            removeAccessToken(token.getValue());
        }

        jdbcTemplate.update(
                 INSERT_ACCESS_TOKEN_SQL
                ,new Object[] {
                         extractTokenKey(token.getValue())
                        ,new SqlLobValue(serializeAccessToken(token))
                        ,authenticationKeyGenerator.extractKey(authentication)
                        ,authentication.isClientOnly() ? null : authentication.getName()
                        ,authentication.getOAuth2Request().getClientId()
                        ,new SqlLobValue(serializeAuthentication(authentication))
                        ,extractTokenKey(refreshToken)
                        ,token.getExpiration()
                }
                ,new int[] {
                         Types.VARCHAR
                        ,Types.BLOB
                        ,Types.VARCHAR
                        ,Types.VARCHAR
                        ,Types.VARCHAR
                        ,Types.BLOB
                        ,Types.VARCHAR
                        ,Types.TIMESTAMP
                 });
    }

    @Override
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {

        jdbcTemplate.update(
                INSERT_REFRESH_TOKEN_SQL
                ,new Object[] {
                         extractTokenKey(refreshToken.getValue())
                        ,new SqlLobValue(serializeRefreshToken(refreshToken))
                        ,new SqlLobValue(serializeAuthentication(authentication))
                        ,DefaultExpiringOAuth2RefreshToken.class.cast(refreshToken).getExpiration()
                }
                ,new int[] {
                         Types.VARCHAR
                        ,Types.BLOB
                        ,Types.BLOB
                        ,Types.TIMESTAMP
                });
    }
}