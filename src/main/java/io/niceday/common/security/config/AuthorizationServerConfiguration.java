package io.niceday.common.security.config;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.common.security.constant.SecurityConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import javax.sql.DataSource;

import static org.springframework.security.oauth2.common.exceptions.OAuth2Exception.INVALID_TOKEN;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description authorization server configuration
 **********************************************************************************************************************/
@Configuration
@EnableAuthorizationServer
@RequiredArgsConstructor
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private final DataSource dataSource;
    private final AuthenticationManager authenticationManager;
    private final CustomJdbcTokenStore  customJdbcTokenStore;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
        configurer.jdbc(dataSource);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer configurer) throws Exception {
        configurer.tokenStore(customJdbcTokenStore)
                .authenticationManager(authenticationManager)
                .tokenServices(tokenServices(configurer));
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer configurer) throws Exception {
        configurer
                .tokenKeyAccess  (SecurityConstant.PERMIT_ALL)
                .checkTokenAccess(SecurityConstant.IS_AUTHENTICATED);
    }

    private AuthorizationServerTokenServices tokenServices(final AuthorizationServerEndpointsConfigurer endpoints) {

        endpoints.exceptionTranslator(exception -> {
            if(exception instanceof NotFoundException){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new OAuth2Exception(exception.getMessage()));
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new OAuth2Exception(INVALID_TOKEN));
        });

        final DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(endpoints.getTokenStore());
        tokenServices.setSupportRefreshToken  (true);
        tokenServices.setReuseRefreshToken    (true);
        tokenServices.setClientDetailsService (endpoints.getClientDetailsService());
        tokenServices.setTokenEnhancer        (endpoints.getTokenEnhancer());
        tokenServices.setAuthenticationManager(authenticationManager);
        return tokenServices;
    }
}
