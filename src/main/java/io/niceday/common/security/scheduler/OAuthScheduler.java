package io.niceday.common.security.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @since       2021.09.16
 * @author      lucas
 * @description oauth scheduler
 **********************************************************************************************************************/
@Slf4j
@Component
@RequiredArgsConstructor
public class OAuthScheduler {

    private final JdbcTemplate jdbcTemplate;

    @Scheduled(fixedRate=1000 * 60 * 5)
    public void removeTokens() {
        int removeAccessTokenCount  = jdbcTemplate.update("delete from oauth_access_token  where expire_at < ?", LocalDateTime.now());
        int removeRefreshTokenCount = jdbcTemplate.update("delete from oauth_refresh_token where expire_at < ?", LocalDateTime.now());
        log.debug("delete expire tokens : access-token-count({}), refresh-token-count({})", removeAccessTokenCount, removeRefreshTokenCount);
    }
}