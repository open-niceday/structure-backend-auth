package io.niceday.common.security.provider;

import io.niceday.common.security.login.adapter.LoginAdapter;
import io.niceday.bali.access.account.entity.Account;
import io.niceday.bali.access.role.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description default pre authentication provider
 *              found for dependency: expected at least 1 bean which qualifies as autowire candidate for this dependency. Dependency annotations
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class DefaultPreAuthenticatedAuthenticationProvider extends PreAuthenticatedAuthenticationProvider {

    private final AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> authenticationUserDetailsService;

    @PostConstruct
    public void init(){
        super.setPreAuthenticatedUserDetailsService(authenticationUserDetailsService);
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String  accountId = authentication.getName();
        String  accountPw = Objects.toString(authentication.getCredentials());
        Account account   = LoginAdapter.getAccount(accountId, accountPw);

        return new PreAuthenticatedAuthenticationToken(account.getEmail(), accountPw, getAuthorities(account.getRoles()));
    }

    public static Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles){
        return Optional.ofNullable(roles).orElse(Collections.emptyList()).stream()
                .map    (role -> new SimpleGrantedAuthority(role.getRoleType().name()))
                .collect(Collectors.toList());
    }
}