package io.niceday.common.security.provider;

import io.niceday.common.engine.constant.Constant;
import io.niceday.common.engine.helper.model.ObjectHelper;
import io.niceday.common.security.config.CustomJdbcTokenStore;
import io.niceday.common.security.login.adapter.LoginAdapter;
import io.niceday.bali.access.account.entity.Account;
import io.niceday.bali.access.role.entity.Role;
import io.niceday.common.security.principal.mapper.PrincipalMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description default authentication provider
 **********************************************************************************************************************/
@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultAuthenticationProvider implements AuthenticationProvider {

    private final CustomJdbcTokenStore  customJdbcTokenStore;

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if(Objects.isNull(authentication.getCredentials()) || StringUtils.isBlank(authentication.getCredentials().toString())){
            throw new UsernameNotFoundException("invalid credentials");
        }

        String  accountId = authentication.getName();
        String  accountPw = Objects.toString(authentication.getCredentials());
        Account account   = LoginAdapter.getAccount(accountId, accountPw);

        return getAuthenticationToken(account);
    }

    private Authentication getAuthenticationToken(Account account){
        String encodedPrincipal = ObjectHelper.toJsonWithEncodeBase64(PrincipalMapper.mapper.toPrincipal(account));
        removeToken(encodedPrincipal);
        return new UsernamePasswordAuthenticationToken(encodedPrincipal, Constant.String.EMPTY, getAuthorities(account.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles){
        return Optional.ofNullable(roles).orElse(Collections.emptyList()).stream()
                .map    (role -> new SimpleGrantedAuthority(role.getRoleType().name()))
                .collect(Collectors.toList());
    }

    private void removeToken(String principal){
        CustomJdbcTokenStore store  = CustomJdbcTokenStore.class.cast(customJdbcTokenStore);
        Collection<OAuth2AccessToken> tokens = store.findTokensByUserName(principal);
        for(OAuth2AccessToken token : Optional.ofNullable(tokens).orElse(Collections.emptyList())){
            Optional.ofNullable(token).ifPresent(t -> {
                store.removeAccessToken (t);
                store.removeRefreshToken(t.getRefreshToken());
            });
        }
    }
}