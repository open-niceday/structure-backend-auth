package io.niceday.common.security.login.adapter;

import io.niceday.bali.access.account.entity.Account;
import io.niceday.bali.access.account.service.AccountService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @since       2020.04.02
 * @author      lucas
 * @description login adapter
 **********************************************************************************************************************/
@Component
public class LoginAdapter {

	private static AccountService  accountService;
	private static PasswordEncoder passwordEncoder;

	@Autowired
	private LoginAdapter(AccountService accountService, PasswordEncoder passwordEncoder) {
		LoginAdapter.accountService  = accountService;
		LoginAdapter.passwordEncoder = passwordEncoder;
	}

	public static Account getAccount(String email, String accountPw){

		Account account = accountService.getByEmail(email).orElseThrow(() -> new UsernameNotFoundException("invalid account"));
		if(BooleanUtils.isFalse(passwordEncoder.matches(accountPw, account.getPassword()))) {
			throw new UsernameNotFoundException("invalid password");
		}

		return account;
	}
}