package io.niceday.common.security.details;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description account details service
 **********************************************************************************************************************/
@Getter
@Setter
@Deprecated
public class AccountDetailsService extends User {

    public AccountDetailsService(String username, String password, Collection roles){
        super(username, password, roles);
    }
}