package io.niceday.common.security.details;

import io.niceday.bali.access.account.entity.Account;
import io.niceday.bali.access.account.service.AccountService;
import io.niceday.bali.access.role.entity.Role;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.CastUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description pre authentication user details service
 **********************************************************************************************************************/
@Service
@Transactional
@NoArgsConstructor
@Deprecated
public class PreAuthenticationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    @Autowired
    private AccountService accountService;

    public static Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles){
        return Optional.ofNullable(roles).orElse(Collections.emptyList()).stream()
                .map    (role -> new SimpleGrantedAuthority(role.getRoleType().name()))
                .collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        Account account = accountService.get(CastUtils.cast(token.getPrincipal()));
        return new AccountDetailsService(account.getEmail(), account.getPassword(), getAuthorities(account.getRoles()));
    }
}