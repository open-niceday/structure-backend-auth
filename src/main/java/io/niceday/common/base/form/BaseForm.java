package io.niceday.common.base.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description base form
 **********************************************************************************************************************/
public class BaseForm {

	public static class Response {

		@Data
		public static class Account {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="이름")
			private String name;
		}
	}
}