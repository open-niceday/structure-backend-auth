package io.niceday.common.base.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.bali.access.account.entity.Account;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

/**   
 * @since       2021.03.25
 * @author      lucas
 * @description base
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Base implements Serializable {

	@Description("등록일시")
	@CreatedDate
	@Column(name="created_at", updatable=false)
	private LocalDateTime createdAt;

	@Description("수정일시")
	@LastModifiedDate
	@Column(name="updated_at", insertable=false)
	private LocalDateTime updatedAt;

	@Description("등록자")
	@CreatedBy
	@JoinColumn(name="created_by", updatable=false)
	@ManyToOne(fetch= FetchType.LAZY)
	private Account creator;

	@Description("수정자")
	@LastModifiedBy
	@JoinColumn(name="updated_by", insertable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	private Account updator;
}