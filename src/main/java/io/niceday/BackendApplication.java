package io.niceday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description backend application
 **********************************************************************************************************************/
@SpringBootApplication
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	} 
}
