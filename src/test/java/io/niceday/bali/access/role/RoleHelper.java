package io.niceday.bali.access.role;

import io.niceday.common.engine.test.TestHelper;
import lombok.SneakyThrows;

import static io.niceday.common.engine.helper.model.ObjectHelper.newInstance;
import static io.niceday.common.engine.helper.model.ObjectHelper.toInstance;
import static io.niceday.common.engine.helper.model.ObjectHelper.toJson;
import static io.niceday.bali.access.role.form.RoleForm.Request;
import static io.niceday.bali.access.role.form.RoleForm.Response;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role helper
 **********************************************************************************************************************/
public class RoleHelper extends TestHelper {

    @SneakyThrows
    public static void get_page(Request.Find find) {
        mock.perform(get  ("/api/roles/pages")
                .content  (toJson(find)))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static void get(Long roleId) {
        mock.perform(get  ("/api/roles/{roleId}", roleId))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static Response.FindOne add(Request.Add add) {
        return toInstance(Response.FindOne.class,
                mock.perform(post ("/api/roles")
                        .content  (toJson(add)))
                        .andExpect(status().isOk())
                        .andDo    (print()));
    }

    @SneakyThrows
    public static Response.FindOne modify(Long roleId, Request.Modify modify) {
        return toInstance(Response.FindOne.class,
                mock.perform(put  ("/api/roles/{roleId}", roleId)
                        .content  (toJson(modify)))
                        .andExpect(status().isOk())
                        .andDo    (print()));
    }

    @SneakyThrows
    public static void remove(Long roleId) {
        mock.perform(delete("/api/roles/{roleId}", roleId))
                .andExpect (status().isOk())
                .andDo     (print());
    }

    @SneakyThrows
    public static void get_not_found(Long roleId) {
        mock.perform(get  ("/api/roles/{roleId}", roleId))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    @SneakyThrows
    public static void modify_not_found(Long roleId, Request.Modify modify) {
        mock.perform(put  ("/api/roles/{roleId}", roleId)
                .content  (toJson(modify)))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    @SneakyThrows
    public static void remove_not_found(Long roleId) {
        mock.perform(delete("/api/roles/{roleId}", roleId))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    public static Request.Find newFind() {
        return newInstance(Request.Find.class);
    }

    public static Request.Add newAdd(Object ... nested) {
        return newInstance(Request.Add.class);
    }

    public static Request.Modify newModify() {
        return newInstance(Request.Modify.class);
    }
}
