package io.niceday.bali.access.role;

import io.niceday.common.engine.test.SuperTest;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.bali.access.role.RoleHelper.add;
import static io.niceday.bali.access.role.RoleHelper.get;
import static io.niceday.bali.access.role.RoleHelper.get_not_found;
import static io.niceday.bali.access.role.RoleHelper.get_page;
import static io.niceday.bali.access.role.RoleHelper.modify;
import static io.niceday.bali.access.role.RoleHelper.modify_not_found;
import static io.niceday.bali.access.role.RoleHelper.newAdd;
import static io.niceday.bali.access.role.RoleHelper.newFind;
import static io.niceday.bali.access.role.RoleHelper.newModify;
import static io.niceday.bali.access.role.RoleHelper.remove;
import static io.niceday.bali.access.role.RoleHelper.remove_not_found;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role test
 **********************************************************************************************************************/
@Transactional
@DisplayName("권한")
public class RoleTest extends SuperTest {

	@Test
	@DisplayName("01_페이지")
	public void t01_get_page() {
		add     (newAdd());
		add     (newAdd());
		get_page(newFind());
	}

	@Test
	@DisplayName("02_조회")
	public void t02_get() {
		get(add(newAdd()).getId());
	}

	@Test
	@DisplayName("03_등록")
	public void t03_add() {
		add(newAdd());
	}

	@Test
	@DisplayName("04_수정")
	public void t04_modify() {
		modify(add(newAdd()).getId(), newModify());
	}

	@Test
	@DisplayName("05_삭제")
	public void t05_remove() {
		remove(add(newAdd()).getId());
	}

	@Test
	@DisplayName("06_조회_NotFound")
	public void t06_get_not_found() {
		get_not_found(Long.MIN_VALUE);
	}

	@Test
	@DisplayName("07_수정_NotFound")
	public void t07_modify_not_found() {
		modify_not_found(Long.MIN_VALUE, newModify());
	}

	@Test
	@DisplayName("08_삭제_NotFound")
	public void t08_remove_not_found() {
		remove_not_found(Long.MIN_VALUE);
	}
}