package io.niceday.bali.access.account;

import io.niceday.common.engine.test.SuperTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static io.niceday.bali.access.account.AccountHelper.add;
import static io.niceday.bali.access.account.AccountHelper.get;
import static io.niceday.bali.access.account.AccountHelper.get_not_found;
import static io.niceday.bali.access.account.AccountHelper.get_page;
import static io.niceday.bali.access.account.AccountHelper.modify;
import static io.niceday.bali.access.account.AccountHelper.modify_not_found;
import static io.niceday.bali.access.account.AccountHelper.newAdd;
import static io.niceday.bali.access.account.AccountHelper.newFind;
import static io.niceday.bali.access.account.AccountHelper.newModify;
import static io.niceday.bali.access.account.AccountHelper.remove;
import static io.niceday.bali.access.account.AccountHelper.remove_not_found;
import static io.niceday.bali.access.account.form.AccountForm.Response;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description account white test
 **********************************************************************************************************************/
@Transactional
@DisplayName("사용자_화이트리스트")
public class AccountWhiteTest extends SuperTest {

    @PersistenceContext
    private EntityManager manager;

    @AfterEach
    public void after(){
        manager.flush();
    }

    @Test
    @DisplayName("01_페이지")
    public void t01_getPage() {
        add(newAdd());
        get_page(newFind());
    }

    @Test
    @DisplayName("02_조회")
    public void t02_get() {
        Response.FindOne account = add(newAdd());
        get(account.getId());
    }

    @Test
    @DisplayName("03_등록")
    public void t03_add() {
        add(newAdd());
    }

    @Test
    @DisplayName("04_수정")
    public void t04_modify() {
        Response.FindOne account = add(newAdd());
        modify(account.getId(), newModify());
    }

    @Test
    @DisplayName("05_삭제")
    public void t05_remove() {
        Response.FindOne account = add(newAdd());
        remove(account.getId());
    }

    @Test
    @DisplayName("06_조회_NotFound")
    public void t06_get_notFound() {
        get_not_found(Long.MIN_VALUE);
    }

    @Test
    @DisplayName("07_수정_NotFound")
    public void t07_modify_notFound() {
        modify_not_found(Long.MIN_VALUE, newModify());
    }

    @Test
    @DisplayName("08_삭제_NotFound")
    public void t08_remove_notFound() {
        remove_not_found(Long.MIN_VALUE);
    }
}