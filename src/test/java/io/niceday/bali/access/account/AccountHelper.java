package io.niceday.bali.access.account;

import io.niceday.common.engine.test.TestHelper;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;

import static io.niceday.common.engine.helper.model.ObjectHelper.newInstance;
import static io.niceday.common.engine.helper.model.ObjectHelper.toInstance;
import static io.niceday.common.engine.helper.model.ObjectHelper.toJson;
import static io.niceday.bali.access.account.form.AccountForm.Request;
import static io.niceday.bali.access.account.form.AccountForm.Response;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description account helper
 **********************************************************************************************************************/
public class AccountHelper extends TestHelper {

    @SneakyThrows
    public static void get_page(Request.Find find) {
        mock.perform(get  ("/api/accounts/pages")
                .content  (toJson(find)))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static void get(Long accountId) {
        mock.perform(get  ("/api/accounts/{accountId}", accountId))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static Response.FindOne add(Request.Add add) {
        return toInstance(Response.FindOne.class,
                mock.perform(post ("/api/accounts")
                        .content  (toJson(add)))
                        .andExpect(status().isOk())
                        .andDo    (print()));
    }

    @SneakyThrows
    public static Response.FindOne modify(Long accountId, Request.Modify modify) {
        return toInstance(Response.FindOne.class,
                mock.perform(put  ("/api/accounts/{accountId}", accountId)
                        .content  (toJson(modify)))
                        .andExpect(status().isOk())
                        .andDo    (print()));
    }

    @SneakyThrows
    public static void remove(Long accountId) {
        mock.perform(delete("/api/accounts/{accountId}", accountId))
                .andExpect (status().isOk())
                .andDo     (print());
    }

    @SneakyThrows
    public static void get_not_found(Long accountId) {
        mock.perform(get  ("/api/accounts/{accountId}", accountId))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    @SneakyThrows
    public static void modify_not_found(Long accountId, Request.Modify modify) {
        mock.perform(put  ("/api/accounts/{accountId}", accountId)
                .content  (toJson(modify)))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    @SneakyThrows
    public static void remove_not_found(Long accountId) {
        mock.perform(delete("/api/accounts/{accountId}", accountId))
                .andExpect(status().isNotFound())
                .andDo    (print());
    }

    public static Request.Find newFind() {
        return newInstance(Request.Find.class);
    }

    public static Request.Add newAdd() {
        return newInstance(Request.Add.class).toBuilder().name(RandomStringUtils.randomAlphabetic(10)).build();
    }

    public static Request.Modify newModify() {
        return newInstance(Request.Modify.class);
    }
}
