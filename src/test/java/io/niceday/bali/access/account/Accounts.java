package io.niceday.bali.access.account;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.19
 * @author      lucas
 * @description accounts
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum Accounts {

     LUCAS (1L,"임현우","lucas@niceday.io" )
    ,MUSHI (2L,"이상윤","mushi@niceday.io" )
    ,MARTIN(3L,"한라산","martin@niceday.io")
    ,HENRY (4L,"박정수","henry@niceday.io" )
    ,VAN   (5L,"배상환","van@niceday.io"   )
    ,CARROT(6L,"정병훈","carrot@niceday.io");

    private Long   id;
    private String name;
    private String email;
}